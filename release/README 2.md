#e-learning-pc2-fis3
在线教育PC版v2 前端

npm # 开发说明

## 前言

项目名称：学成网
项目描述：学成网，

## git版本控制

```bash
mkdir travel-pc-fis3
cd travel-pc-fis3
git init
touch README.md
git add README.md
git commit -m "first commit"
git remote add origin https://git.oschina.net/docafe/e_learning_pc_fis3.git
git push -u origin master

or

git clone https://git.oschina.net/docafe/e_learning_pc_fis3.git
```

## 安装

```bash
npm i
bower i
```

## 运行

```bash
npm start 自动更新www目录
npm run open 打开预览www目录
npm run release 手动更新www目录
npm run dist 发布release 到目录./release
```

> 注：npm start后，刷新下打开的浏览器，下次修改浏览器会自动刷新

## .gitignore

```bash
*.DS_Store
*.log
*.zip
.idea
.sass-cache/
node_modules/
bower_components/
release/
```

## UI参考

http://class.hujiang.com/

## 页面和组件规则

- 每个页面/组件为单位，创建独立目录，所有用到的资源都放在这个目录下
- 组件间不要存在复用关系、不互相依赖

## 页面page++++++++++++

页面名称 | 说明
----------------------------------------------------------------------|--------------
pages/learing-index/learing-index.html                                | 学成网首页
pages/learing-list/learing-list.html                                  | 学成网列表
pages/learing-article/learing-article.html                            | 学成网详情
pages/learing-teacher-info/learing-teacher-info.html                  | 学成网老师详情
pages/learing-problem-feedback/learing-problem-feedback.html          | 学成网问题反馈
pages/learing-teacher-info/learing-teacher-info.html                  | 学成网支付
pages/learing-course-answer                                           | 课程学习-问题页
pages/learing-course-document                                         | 课程学习-阅读学习
pages/learing-course-problem                                          | 课程学习-答案页
pages/learing-course-video                                            | 课程学习-视频学习





## 组件widget

- widget列表

页面名称 | 说明
---------------------------------------------------------------------------------------|--------------
modules/ui-modules/header/header.html                                                  | 整站页头
modules/ui-modules/footer/footer.html                                                  | 整站页尾
modules/ui-modules/travel-index-imgroll                                                | 首页banner图轮播
modules/ui-modules/tranel-index-nav                                                    | 首页banner左边tab选项框
modules/ui-modules/learing-conten-list                                                 | 首页内容部分
modules/ui-modules/learing-index-bot-video                                             | 首页底部运作方式部分
modules/ui-modules/learing-index-cont-nav                                              | 首页侧边导航
modules/ui-modules/learing-list-cont-left                                              | 列表页内容左边部分
modules/ui-modules/learing-list-cont-right                                             | 列表页右边推荐部分
modules/ui-modules/learing-list-title                                                  | 列表页顶部选择部分
modules/ui-modules/learing-recommend                                                   | 首页大学导航





- 调用方式

```html
<link rel="import" href="../../modules/ui-modules/header/header.html?__inline">
```

> 以内容嵌入的方式, 在文件名后面添加 `?__inline`


