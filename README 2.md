#e-learning-pc2-fis3
在线教育PC版v2 前端

npm # 开发说明

## 前言

项目名称：学成网
项目描述：学成网，

## git版本控制

```bash
mkdir travel-pc-fis3
cd travel-pc-fis3
git init
touch README.md
git add README.md
git commit -m "first commit"
git remote add origin https://git.oschina.net/docafe/e_learning_pc_fis3.git
git push -u origin master

or

git clone https://git.oschina.net/docafe/e_learning_pc_fis3.git
```

## 安装

```bash
npm i
bower i
```

## 运行

```bash
npm start 自动更新www目录
npm run open 打开预览www目录
npm run release 手动更新www目录
npm run dist 发布release 到目录./release
```

> 注：npm start后，刷新下打开的浏览器，下次修改浏览器会自动刷新

## .gitignore

```bash
*.DS_Store
*.log
*.zip
.idea
.sass-cache/
node_modules/
bower_components/
release/
```

## UI参考

http://class.hujiang.com/

## 页面和组件规则

- 每个页面/组件为单位，创建独立目录，所有用到的资源都放在这个目录下
- 组件间不要存在复用关系、不互相依赖

## 页面page++++++++++++

页面名称 | 说明
----------------------------------------------------------------------|--------------
p





## 组件widget

- widget列表

页面名称 | 说明
---------------------------------------------------------------------------------------|--------------




- 调用方式

```html
<link rel="import" href="../../modules/ui-modules/header/header.html?__inline">
```

> 以内容嵌入的方式, 在文件名后面添加 `?__inline`


